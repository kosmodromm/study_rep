# Carousel component

## Prerequisites

- Git - [Download & Install Git](https://git-scm.com/downloads).
- Node.js - [Download & Install Node.js](https://nodejs.org/en/download/) and the npm package manager.

## Downloading

```
git clone {repository URL}
```

## Installing NPM modules

```
npm install
```

## Running application

```
npm start
```

App starting on port (8000 as default). You can open it
manually by typing http://localhost:8080/.

## Description

A component that can be inserted into the other application.
Images are used as an example, but it can be any array of HTML elements. File `items.js` in the project root.

![](https://i.gyazo.com/09c250063087d488e7f48b256565a1b4.png)

---

The carousel supports **swipes**:

![](https://i.ibb.co/Y7kZMDT/swipes.gif)

---

Сhanges the size and visibility of elements depending on the width of the screen:

![](https://i.ibb.co/G3cc8Gw/adapt.gif)

---

Supports **infinite scrolling**:

![](https://i.ibb.co/5RBbzr3/infinite.gif)

---

Supports scrolling to **selected slide**:

![](https://i.ibb.co/W6pjTSV/selected-Slide.gif)
