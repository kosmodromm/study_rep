import React from "react";
import "./Carousel.css";
import Gallery from "./components/Gallery.jsx";
import NavArrows from "./components/NavArrows.jsx";

class Carousel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentElem: 0,
    };

    this.bindMethods();
  }

  bindMethods() {
    this.moveCarousel = this.moveCarousel.bind(this);
    this.moveCarouselToSlide = this.moveCarouselToSlide.bind(this);
    this.onGalleryTransition = this.onGalleryTransition.bind(this);
  }

  moveCarousel(direction) {
    if (direction === "left") {
      this.setState({ currentElem: this.state.currentElem - 1 });
    } else if (direction === "right") {
      this.setState({ currentElem: this.state.currentElem + 1 });
    }
  }

  moveCarouselToSlide(index) {
    this.setState({ currentElem: index });
  }

  onGalleryTransition(e) {
    let currentElem;
    if (this.state.currentElem < 0) {
      currentElem = this.props.items.length - 1;
    } else if (this.state.currentElem > this.props.items.length - 1) {
      currentElem = 0;
    } else {
      return;
    }
    e.target.style.transition = "none";
    this.setState({ currentElem }, () => {
      setTimeout(() => {
        e.target.style.transition = "transform 0.3s ease-in-out";
      }, 10);
    });
  }

  render() {
    return (
      <div className="carousel">
        <Gallery
          currentElem={this.state.currentElem}
          items={this.props.items}
          moveCarousel={this.moveCarousel}
          moveCarouselToSlide={this.moveCarouselToSlide}
          onGalleryTransition={this.onGalleryTransition}
        />
        <NavArrows
          itemsCount={this.props.items.length}
          currentElem={this.state.currentElem}
          moveCarousel={this.moveCarousel}
        />
      </div>
    );
  }
}

export default Carousel;
