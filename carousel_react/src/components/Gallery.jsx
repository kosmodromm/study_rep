import React from "react";

// let initialPosition = null;
class Gallery extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      additionalTranslate: 0,
    };

    this.initialPosition = null;

    this.bindMethods();
  }

  bindMethods() {
    this.getTranslateValue = this.getTranslateValue.bind(this);
    this.handlePointerDown = this.handlePointerDown.bind(this);
    this.handlePointerUp = this.handlePointerUp.bind(this);
    this.stopDefAction = this.stopDefAction.bind(this);
  }

  stopDefAction(e) {
    e.preventDefault();
  }

  handlePointerDown(e) {
    this.initialPosition = e.pageX;
  }

  handlePointerUp(e) {
    this.setState({ additionalTranslate: 0 });
    const currentPosition = e.pageX;
    const diff = currentPosition - this.initialPosition;
    if (diff > 50) {
      this.props.moveCarousel("left");
      this.initialPosition = null;
    } else if (diff < -50) {
      this.props.moveCarousel("right");
      this.initialPosition = null;
    }
  }

  getTranslateValue() {
    let translate;
    if (window.innerWidth > 1010) {
      translate = 200 - (this.props.currentElem + 1) * 620;
    } else {
      translate = -(this.props.currentElem + 1) * 600;
    }
    translate += this.state.additionalTranslate;
    return { transform: `translateX(${translate}px)` };
  }

  cloneDeep(arr) {
    const newArr = [];
    for (let i = 0; i < arr.length; i++) {
      newArr.push({ ...arr[i] });
    }
    return newArr;
  }

  render() {
    return (
      <div className="gallery">
        <div>
          <ul
            className="galleryList"
            onPointerDown={this.handlePointerDown}
            onPointerUp={this.handlePointerUp}
            onTransitionEnd={this.props.onGalleryTransition}
            style={this.getTranslateValue()}
          >
            <li className="galleryItem" onPointerDown={this.stopDefAction}>
              {this.props.items[this.props.items.length - 1]}
            </li>
            {this.props.items.map((e, i) => (
              <li
                key={i}
                className="galleryItem"
                onPointerDown={this.stopDefAction}
              >
                {e}
              </li>
            ))}
            <li className="galleryItem" onPointerDown={this.stopDefAction}>
              {this.props.items[0]}
            </li>
          </ul>
        </div>
        <div>
          <ul className="gallery-switchers">
            {this.props.items.map((e, i) => (
              <li
                key={i}
                className={`switcher ${
                  i === this.props.currentElem ? "active" : ""
                }`}
                onClick={() => this.props.moveCarouselToSlide(i)}
              ></li>
            ))}
          </ul>
        </div>
      </div>
    );
  }
}

export default Gallery;
