import React from "react";

class NavArrows extends React.Component {
  render() {
    return (
      <div className="nav-arrows">
        <button
          className="arrow"
          style={{ backgroundImage: "url(/src/components/previous.png)" }}
          onClick={() => this.props.moveCarousel("left")}
        ></button>
        <button
          className="arrow"
          style={{ backgroundImage: "url(/src/components/next.png)" }}
          onClick={() => this.props.moveCarousel("right")}
        ></button>
      </div>
    );
  }
}

export default NavArrows;
