import React from "react";
import ReactDOM from "react-dom";
import Carousel from "./Carousel.jsx";
import items from "../items";

ReactDOM.render(<Carousel items={items} />, document.getElementById("root"));
